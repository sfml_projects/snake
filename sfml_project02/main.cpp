
#include "main.h"


#define WIDTH 500
#define HEIGHT 500
#define SQUARE_SHAPE 10
#define SPEED  10


void moveSnake(const sf::Vector2<int> & direction, std::list<sf::RectangleShape> & snake) 
{
	//(1,0) ( -1,0) ( 0, 1), (0 -1)

	//left
	if (direction.x < 0) 
	{
		sf::Vector2f pos(snake.front().getPosition().x - SQUARE_SHAPE, snake.front().getPosition().y);
		
		sf::RectangleShape nFront(sf::Vector2f(10, 10));
		nFront.setFillColor(sf::Color::Green);
		nFront.setPosition(pos);
		nFront.setOutlineColor(sf::Color::Blue);
		nFront.setOutlineThickness(2);

		snake.push_front(
			{
				nFront
			}
		);

		snake.pop_back();
	}

	//right
	if (direction.x > 0)
	{
		sf::Vector2f pos(snake.front().getPosition().x + SQUARE_SHAPE, snake.front().getPosition().y);

		sf::RectangleShape nFront(sf::Vector2f(10, 10));
		nFront.setFillColor(sf::Color::Green);
		nFront.setPosition(pos);
		nFront.setOutlineColor(sf::Color::Blue);
		nFront.setOutlineThickness(2);

		snake.push_front(
			{
				nFront
			}
		);

		snake.pop_back();
	}

	//up
	if (direction.y > 0)
	{
		sf::Vector2f pos(snake.front().getPosition().x, snake.front().getPosition().y + SQUARE_SHAPE);

		sf::RectangleShape nFront(sf::Vector2f(10, 10));
		nFront.setFillColor(sf::Color::Green);
		nFront.setPosition(pos);

		snake.push_front(
			{
				nFront
			}
		);

		snake.pop_back();
	}

	//down
	if (direction.y < 0)
	{
		sf::Vector2f pos(snake.front().getPosition().x, snake.front().getPosition().y - SQUARE_SHAPE);

		sf::RectangleShape nFront(sf::Vector2f(10, 10));
		nFront.setFillColor(sf::Color::Green);
		nFront.setPosition(pos);

		snake.push_front(
			{
				nFront
			}
		);

		snake.pop_back();
	}

	

	/*if (direction.x > 0) 
	{
		float x = sq.x + sq.getSize() / 3; ;
		
		if (x > WIDTH) 
		{
			x = 0;
		}
		
		sq.shape.setPosition(x , sq.y);
		sq.x = x ;
	}

	if (direction.x < 0) 
	{
		float x = sq.x - sq.getSize()/3;
		
		if (x < 0)
		{
			x = WIDTH;
		}

		sq.shape.setPosition(x, sq.y);
		sq.x = x;
	}


	if (direction.y > 0)
	{
		float y = sq.y + sq.getSize() / 3;;

		if (y > HEIGHT)
		{
			y = 0;
		}

		sq.shape.setPosition(sq.x, y);
		sq.y = y;
	}

	if (direction.y < 0)
	{
		float y = sq.y - sq.getSize() / 3;;

		if (y > HEIGHT)
		{
			y = 0;
		}

		sq.shape.setPosition(sq.x, y);
		sq.y = y;
	}*/

}


void setupSnake(std::list<sf::RectangleShape> & snake) 
{

	snake.clear();

	sf::RectangleShape front(sf::Vector2f(10, 10));
	front.setPosition(WIDTH / 2, HEIGHT / 2);
	front.setFillColor(sf::Color::Green);
	front.setOutlineColor(sf::Color::Blue);
	front.setOutlineThickness(2);
	snake.push_front(front);

	sf::RectangleShape last(sf::Vector2f(10, 10));
	last.setPosition(sf::Vector2f(snake.front().getPosition().x + last.getSize().x, snake.front().getPosition().y));
	last.setFillColor(sf::Color::Green);
	last.setOutlineColor(sf::Color::Blue);
	last.setOutlineThickness(2);
	snake.push_back(last);
}

int main() 
{
	sf::Clock clock;
	sf::RenderWindow window(sf::VideoMode(500, 500), "SFML Snake!", sf::Style::Titlebar | sf::Style::Close);
	window.setFramerateLimit(30);
	window.setVerticalSyncEnabled(true);
	window.setKeyRepeatEnabled(false);
	bool bDead = false;
	//defining initial snake
	std::list<sf::RectangleShape> snake;
	
	setupSnake(snake);
	
	sf::Vector2i direction(0, 0);

	sf::RectangleShape item(sf::Vector2f(10, 10));
	item.setFillColor(sf::Color::Red);
	item.setPosition(200, 200);
	float lastCycle = 0;
	while (window.isOpen()) 
	{
		while (!bDead) 
		{
			sf::Time elapsed = clock.getElapsedTime();
			

			float deltaTime = 1 / (float)elapsed.asMilliseconds();
			std::cout << deltaTime << std::endl;
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed) 
				{
					window.close();
					bDead = true;
				}

				if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::Left && direction.x == 0)
					{
						direction.x = -1;
						direction.y = 0;
					}

					if (event.key.code == sf::Keyboard::Right && direction.x == 0)
					{
						direction.x = 1;
						direction.y = 0;
					}

					if (event.key.code == sf::Keyboard::Up && direction.y == 0)
					{
						direction.x = 0;
						direction.y = -1;
					}

					if (event.key.code == sf::Keyboard::Down && direction.y == 0)
					{
						direction.x = 0;
						direction.y = 1;
					}
				}

			}

			window.clear();
			//window.draw(cShape);
			
			if (elapsed.asSeconds() - lastCycle >= 0.1f) 
			{
				moveSnake(direction, snake);
				lastCycle = elapsed.asSeconds();
			}
			


			if (snake.front().getPosition().x == item.getPosition().x && snake.front().getPosition().y == item.getPosition().y)
			{
				for (int i = 0; i < 2; i++)
				{
					snake.push_back(snake.back());
				}

				int x = std::rand() % WIDTH;
				int y = std::rand() % HEIGHT;

				x = (x % 10 > 5) ? x + (10 - (x % 10)) : x - (x % 10);
				y = (y % 10 > 5) ? y + (10 - (y % 10)) : y - (y % 10);
				while (std::find_if(snake.begin(), snake.end(), [&](auto p) { return p.getPosition().x == x && p.getPosition().y; }) !=
					snake.end()) 
				{
					x = std::rand() % WIDTH;
					y = std::rand() % HEIGHT;
					
					x = (x % 10 > 5) ? x + (10 - (x % 10)) : x - (x % 10);
					y = (y % 10 > 5) ? y + (10 - (y % 10)) : y - (y % 10);
				}

				item.setPosition(sf::Vector2f(x, y));
			}

			auto it = std::find_if(++snake.begin(), snake.end(),
				[&](auto & p) {
					return
						snake.front().getPosition().x == p.getPosition().x && snake.front().getPosition().y == p.getPosition().y; });

			if (it != snake.end())
				bDead = true;

			window.draw(item);

			for (auto & s : snake)
			{
				window.draw(s);
			}
			//window.draw(snake[0].shape);
			window.display();		
		}
		
		setupSnake(snake);
		bDead = false;
		
	
	}
	return 0; 
}